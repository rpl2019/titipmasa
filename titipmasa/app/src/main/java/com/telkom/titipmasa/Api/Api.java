package com.telkom.titipmasa.Api;

import android.widget.EditText;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface Api {

    @FormUrlEncoded
    @POST("userregister")
    Call<ResponseBody> createUser(
            @Field("name") EditText name,
//            @Field("username") EditText username,
            @Field("email") EditText email,
            @Field("password") EditText password
    );




}
