package com.telkom.titipmasa;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.telkom.titipmasa.Api.RetrofitClient;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity  {

    EditText nama, username, email, password, konfirmasiPassword;
    Button daftar;
    boolean isEmptyFields  = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

////////////////////////////////////////////////

    daftar = findViewById(R.id.btn_daftar);

    daftar.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            validasi();
            Log.d("button", "onClick: klikk");
            if (!isEmptyFields){
                Log.d("button", "onClick: klil");
                Toast.makeText(getApplicationContext(), "berhasil Daftar", Toast.LENGTH_SHORT);
            }

        }
    });
  /////////////////////////////////////////////////
    }


    ////////////////////////////////////////////

    public void validasi(){
        nama = findViewById(R.id.edt_nama);
        username = findViewById(R.id.edt_username);
        email = findViewById(R.id.edt_email);
        password = findViewById(R.id.edt_password);
        konfirmasiPassword = findViewById(R.id.edt_konfPassword);


        String inputNama, inputUsername, inputEmail, inputPassword, inputKonfirmasiPassword;

        inputNama = nama.getText().toString().trim();
//        inputUsername = username.getText().toString().trim();
        inputEmail = email.getText().toString().trim();
        inputPassword = password.getText().toString().trim();
        inputKonfirmasiPassword = konfirmasiPassword.getText().toString().trim();



//        if (TextUtils.isEmpty(inputNama)){
//            isEmptyFields  = true;
//            nama.setError("Field ini tidak boleh kosong");
//        }
//        if (TextUtils.isEmpty(inputUsername)){
//            isEmptyFields  = true;
//            username.setError("Field ini tidak boleh kosong");
//        }
//        if (TextUtils.isEmpty(inputEmail)){
//            isEmptyFields  = true;
//            email.setError("Field ini tidak boleh kosong");
//        }
//        if (TextUtils.isEmpty(inputPassword)){
//            isEmptyFields  = true;
//            password.setError("Field ini tidak boleh kosong");
//        }
//        if (TextUtils.isEmpty(inputKonfirmasiPassword)){
//            isEmptyFields  = true;
//            konfirmasiPassword.setError("Field ini tidak boleh kosong");
//        }

        Call<ResponseBody> call = (Call<ResponseBody>) RetrofitClient
                .getInstance()
                .getApi()
                .createUser(inputNama, inputEmail, inputPassword);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    String s = response.body().string();
                    Log.d("test", "onResponse: testt");
                    Toast.makeText(RegisterActivity.this, s, Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            /* Do user registration using the api call*/
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(RegisterActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }

    }

