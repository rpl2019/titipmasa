package com.example.android.titipmasa;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;
import id.titipmasa.model.DataTrip;

public class PostTrip extends AppCompatActivity {

    ArrayList<DataTrip> data;
    DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;
    EditText nama_traveller, tgl_kembali, lokasi;
    Spinner tujuan;

    DatabaseReference database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_trip);


        tujuan = findViewById(R.id.pilih_tujuan_trip);
        lokasi = findViewById(R.id.edt_lokasi_pulang);
        tgl_kembali = findViewById(R.id.tgl_pulang_edit);
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        Button btn_date = findViewById(R.id.btn_date);
        database = FirebaseDatabase.getInstance().getReference("Trips");


        btn_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateDialog();
            }
        });



    }

    public void addTrip(){
        String nama, tujuan_traveller, lokasi_pulang,tglkemlbali;


        tujuan_traveller = tujuan.getSelectedItem().toString();
        lokasi_pulang = lokasi.getText().toString().trim();
        tglkemlbali = tgl_kembali.getText().toString().trim();

        if (!TextUtils.isEmpty(tujuan_traveller)) {

            String id = database.push().getKey();

//            DataTrip dataTrip = new DataTrip(id, nama, tujuan_traveller,tglkemlbali,lokasi_pulang);

//            database.child(tujuan_traveller).child(id).setValue(dataTrip);

            nama_traveller.setText("");
            lokasi.setText("");
            tgl_kembali.setText("");

            Toast.makeText(this, "Post Sukses dibuat", Toast.LENGTH_LONG).show();
        } else {
            //if the value is not given displaying a toast
            Toast.makeText(this, "Please enter a name", Toast.LENGTH_LONG).show();
        }


    }

    private void showDateDialog(){

        Calendar newCalendar = Calendar.getInstance();
        tgl_kembali = findViewById(R.id.tgl_pulang_edit);
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                tgl_kembali.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.show();
    }

    public void posTrip(View view){
        addTrip();
    }


}

