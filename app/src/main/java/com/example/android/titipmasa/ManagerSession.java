package com.example.android.titipmasa;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class ManagerSession {
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Context context;

    private final int SESSION_MODE = 0;

    private static final String PREF_NAME = "LoginSession"; //Session Name
    private static final String IS_LOGIN  = "IsLoggedIn"; //For validate log in.

    //public static final String KEY_USERID = "UserID";
    public static final String KEY_USERMAIL = "UserEmail";
    public static final String KEY_TOKEN    = "Token";

    public ManagerSession(Context context){
        this.context = context;
        sharedPreferences = context.getSharedPreferences(PREF_NAME, SESSION_MODE);
        editor = sharedPreferences.edit();
    }

    public void createLoginSession(String usermail, String token){
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_USERMAIL, usermail);
        editor.putString(KEY_TOKEN, token);
        editor.commit();
    }

    public void loginCheck(){
        if(!this.isLoggedIn()){
            context.startActivity(new Intent(context, ActivityMain.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            ((Activity)context).finish();
        }

        context.startActivity(new Intent(context, ActivityProfile.class));
        ((Activity)context).finish();
    }

    public Boolean isLoggedIn(){
        return sharedPreferences.getBoolean(IS_LOGIN, true);
    }

    public String getKeyToken(){
        String token = sharedPreferences.getString("Token", null);

        return token;
    }
}
