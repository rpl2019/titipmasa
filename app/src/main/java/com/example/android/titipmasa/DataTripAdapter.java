package com.example.android.titipmasa;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import id.titipmasa.model.DataTrip;

public class DataTripAdapter extends RecyclerView.Adapter<DataTripAdapter.TripViewHolder> {

    ArrayList<DataTrip> listTrip;
    Context context;

    public DataTripAdapter(ArrayList<DataTrip> listTrip, Context context) {
        this.listTrip = listTrip;
        this.context = context;
    }

    public ArrayList<DataTrip> getListTrip() {
        return listTrip;
    }

    @NonNull
    @Override
    public TripViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_traveller_lokal,parent,false);

        return new TripViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TripViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class TripViewHolder extends RecyclerView.ViewHolder {
//        TextView
        public TripViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
