package com.example.android.titipmasa;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

public class NavigationBar extends AppCompatActivity {



    final Fragment fragment1 = new FragmentTripLokal();
    final FragmentManager fm = getSupportFragmentManager();
    private ImageView makasar, malang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_bar);

        setTitle("Trip Lokal");
        loadFragment(new FragmentTripLokal());

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment = null;
//            getSupportActionBar().show();
            switch (item.getItemId()) {
                case R.id.nav_lokal:
                    setTitle("Trip Lokal");
                    fragment = new FragmentTripLokal();
                    break;
                case R.id.nav_internasional:
                    setTitle("Trip Internasional");
                    fragment = new FragmentTripInternasional();
                    break;

                case R.id.nav_post:
                    setTitle("Buat Trip");
                    Intent intent = new Intent(NavigationBar.this, PostTripTraveller.class);
                    startActivity(intent);
                    break;

                case R.id.nav_chat:
                    setTitle("Pesan Masuk");
                    fragment = new PesanActivity();
                    break;

                case R.id.nav_profil:
                    setTitle("Profil");
//                    getSupportActionBar().hide();
                    fragment = new FragmentProfil();
                    break;


            }
            return loadFragment(fragment);
        }
    };



    private boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }
    public void makasar(View view){

        Intent intent = new Intent(NavigationBar.this, DisplayTravellerLokal.class);
        intent.putExtra("TEMPAT_TUJUAN", "Makasar");
        startActivity(intent);
    }

    public void Malang(View view){

        Intent intent = new Intent(NavigationBar.this, DisplayTravellerLokal.class);
        intent.putExtra("TEMPAT_TUJUAN", "Malang");
        startActivity(intent);
    }

    public void aceh(View view){

        Intent intent = new Intent(NavigationBar.this, DisplayTravellerLokal.class);
        intent.putExtra("TEMPAT_TUJUAN", "Aceh");
        startActivity(intent);
    }

    public void bali(View view){

        Intent intent = new Intent(NavigationBar.this, DisplayTravellerLokal.class);
        intent.putExtra("TEMPAT_TUJUAN", "Bali");
        startActivity(intent);
    }

    public void wamena(View view){

        Intent intent = new Intent(NavigationBar.this, DisplayTravellerLokal.class);
        intent.putExtra("TEMPAT_TUJUAN", "Wamena");
        startActivity(intent);
    }

    public void manado(View view){

        Intent intent = new Intent(NavigationBar.this, DisplayTravellerLokal.class);
        intent.putExtra("TEMPAT_TUJUAN", "Manado");
        startActivity(intent);
    }

    public void palembang(View view){

        Intent intent = new Intent(NavigationBar.this, DisplayTravellerLokal.class);
        intent.putExtra("TEMPAT_TUJUAN", "Palembang");
        startActivity(intent);
    }

    public void surabaya(View view){

        Intent intent = new Intent(NavigationBar.this, DisplayTravellerLokal.class);
        intent.putExtra("TEMPAT_TUJUAN", "Surabaya");
        startActivity(intent);
    }
    public void bandung(View view){

        Intent intent = new Intent(NavigationBar.this, DisplayTravellerLokal.class);
        intent.putExtra("TEMPAT_TUJUAN", "Bandung");
        startActivity(intent);
    }

    public void jakarta(View view){

        Intent intent = new Intent(NavigationBar.this, DisplayTravellerLokal.class);
        intent.putExtra("TEMPAT_TUJUAN", "Jakarta");
        startActivity(intent);
    }

    public void polandia(View view){

        Intent intent = new Intent(NavigationBar.this, DisplayTravellerLokal.class);
        intent.putExtra("TEMPAT_TUJUAN", "Polandia");
        startActivity(intent);
    }

    public void palestina(View view){

        Intent intent = new Intent(NavigationBar.this, DisplayTravellerLokal.class);
        intent.putExtra("TEMPAT_TUJUAN", "Palestina");
        startActivity(intent);
    }

    public void inggris(View view){

        Intent intent = new Intent(NavigationBar.this, DisplayTravellerLokal.class);
        intent.putExtra("TEMPAT_TUJUAN", "Inggris");
        startActivity(intent);
    }

    public void jerman(View view){

        Intent intent = new Intent(NavigationBar.this, DisplayTravellerLokal.class);
        intent.putExtra("TEMPAT_TUJUAN", "Jerman");
        startActivity(intent);
    }

    public void DisplayPesan(View view){

        Intent intent = new Intent(NavigationBar.this, DetailPesan.class);
        startActivity(intent);
    }

    public void TripSaya(View view){
        Intent intent = new Intent(NavigationBar.this, TripSayaActivity.class);
        startActivity(intent);
    }

    public void TitipanMasuk(View view){
        Intent intent = new Intent(NavigationBar.this, TitipanMasukActivity.class);
        startActivity(intent);
    }

    public void TitipanSaya(View view){
        Intent intent = new Intent(NavigationBar.this, TitipanDiterimaActivity.class);
        startActivity(intent);
    }

    public void DataPribadi(View view){
        Intent intent = new Intent(NavigationBar.this, DataPribadiActivity.class);
        startActivity(intent);
    }

}
