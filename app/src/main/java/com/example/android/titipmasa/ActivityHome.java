package com.example.android.titipmasa;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

public class ActivityHome extends AppCompatActivity {
    BottomNavigationView navView;
    private ActionBar toolbar;
    ImageView makasar;
    TextView mkr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        toolbar = getSupportActionBar();

//        navView = findViewById(R.id.navView);

        makasar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityHome.this, DisplayTravellerLokal.class);
                intent.putExtra("tujuan", "MKR");
                startActivity(intent);
            }
        });

//        navViewListener();

//        fragmentHandler(new FragmentTripLokal());
    }

//    protected void navViewListener(){
//        navView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
//            @Override
//            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
//                Fragment fragment;
//                switch(menuItem.getItemId()){
//                    case R.id.nav_lokal:
//                        toolbar.setTitle("Trip Lokal");
//                        fragment = new FragmentTripLokal();
//                        fragmentHandler(fragment);
//                        return true;
//                    case R.id.nav_internasional:
//                        toolbar.setTitle("Trip Internasional");
//                        fragment = new FragmentTripInternasional();
//                        fragmentHandler(fragment);
//                        return true;
//                    case R.id.nav_keranjang:
//                        toolbar.setTitle("Pesan");
//                        fragment = new FragmentKeranjang();
//                        fragmentHandler(fragment);
//                        return true;
//                    case R.id.nav_profil:
//                        toolbar.setTitle("Profil");
//                        fragment = new FragmentProfil();
//                        fragmentHandler(fragment);
//
//
//                        startActivity(new Intent(ActivityHome.this, ActivityProfile.class));
//                        return true;
//                }
//                return false;
//            }
//        });
//    }

//    protected void fragmentHandler(Fragment fragment){
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.replace(R.id.frame_container, fragment);
//        transaction.addToBackStack(null);
//        transaction.commit();
//    }
}
