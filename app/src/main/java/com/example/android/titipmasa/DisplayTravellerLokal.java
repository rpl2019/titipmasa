package com.example.android.titipmasa;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import id.titipmasa.adapter.TravellerAdapter;
import id.titipmasa.model.DataTrip;

//import androidx.appcompat.app.AppCompatActivity;

public class DisplayTravellerLokal extends AppCompatActivity {
    CardView cardView;
    TextView traveller_lokal;
    ImageView img_profil;
    private RecyclerView rvListTraveller;
    private ArrayList<DataTrip> listData = new ArrayList<>();
    TravellerAdapter adapter;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference reference, dfUsers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.daftar_traveller);
//        Toolbar toolbar = findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        String tempat = intent.getStringExtra("TEMPAT_TUJUAN");

        rvListTraveller = findViewById(R.id.rv_daftar_traveller);
        rvListTraveller.setLayoutManager(new LinearLayoutManager(this));


        reference = FirebaseDatabase.getInstance().getReference();

        Query query = reference.child("Trips").orderByChild("tujuan").equalTo(tempat);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){
                    DataTrip card = dataSnapshot1.getValue(DataTrip.class);
                    listData.add(card);
                }

                adapter = new TravellerAdapter(DisplayTravellerLokal.this, listData);
                adapter.notifyDataSetChanged();
                rvListTraveller.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }

}
