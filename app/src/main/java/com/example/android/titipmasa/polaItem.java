package com.example.android.titipmasa;

import androidx.appcompat.app.AppCompatActivity;

public class polaItem extends AppCompatActivity {
    private String nama, harga;
    private int kota, avatar;

    public polaItem(String nama, String harga, int kota) {
        this.nama = nama;
        this.harga = harga;
        this.kota = kota;
    }

    public String getNama() {
        return nama;
    }
    public String getharga() {
        return harga;
    }
    public int getKota() {
        return kota;
    }
    public int getAvatar() {
        return avatar;
    }

}
