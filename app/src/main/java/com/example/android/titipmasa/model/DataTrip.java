package com.example.android.titipmasa.model;

public class DataTrip {
    String nama;
    String pulang;
    String lokasi;
    String tujuan, id;

    public DataTrip(){

    }


    public DataTrip(String id,String nama,String tujuan, String pulang, String lokasi) {
        this.nama = nama;
        this.pulang = pulang;
        this.lokasi = lokasi;
        this.tujuan = tujuan;
        this.id = id;
    }


    public String getNama() {
        return nama;
    }

    public String getId() {
        return id;
    }

    public String getPulang() {
        return pulang;
    }

    public String getLokasi() {
        return lokasi;
    }

    public String getTujuan() {
        return tujuan;
    }



}
