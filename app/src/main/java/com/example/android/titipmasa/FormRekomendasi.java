package com.example.android.titipmasa;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import androidx.appcompat.app.AppCompatActivity;
import id.titipmasa.model.RekomendasiBarang;

public class FormRekomendasi extends AppCompatActivity {

    EditText namaBarang, hargaBarang, keterangan;
    ImageView pilihGambar;
    Button submitRekomendasi;
    DatabaseReference mDbref;
    StorageReference storageReference;
    FirebaseAuth auth;
    private static final int PICK_IMAGE = 100;
    ImageView imageUri;
    private ProgressDialog progressDialog;
    Uri imgUrl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_rekomendasi);
        setTitle("Form Rekomendasi Barang");

        final String id_user = FirebaseAuth.getInstance().getCurrentUser().getUid();
        namaBarang = findViewById(R.id.edt_nBarangRekom);
        hargaBarang = findViewById(R.id.edt_harga_barang_rekom);
        keterangan = findViewById(R.id.edt_keterangan_rekom);
        imageUri = findViewById(R.id.add_image);
        submitRekomendasi = findViewById(R.id.btn_submit_rekom);
        progressDialog = new ProgressDialog(this);
        storageReference = FirebaseStorage.getInstance().getReference("RekomendasiBarang");
        mDbref = FirebaseDatabase.getInstance().getReference("RekomendasiBarang");
        String id_trip = getIntent().getStringExtra("ID_TRIP");

        submitRekomendasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.setMessage("Proses...");
                progressDialog.show();

               final StorageReference imageBarang = storageReference.child(System.currentTimeMillis() + "." + getFileExtension(imgUrl));

               final String nama_barang = namaBarang.getText().toString().trim();
               final String harga = hargaBarang.getText().toString().trim();
               final String ket = keterangan.getText().toString().trim();

               imageBarang.putFile(imgUrl).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                   @Override
                   public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                       imageBarang.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                           @Override
                           public void onSuccess(Uri uri) {
                               String urlImageBarang = uri.toString().trim();

                               String id = mDbref.push().getKey();
                               RekomendasiBarang rekomendasiBarang = new RekomendasiBarang(id, id_user, nama_barang, harga, urlImageBarang, ket);

                               mDbref.child(id).setValue(rekomendasiBarang);
                               progressDialog.dismiss();
                               Toast.makeText(FormRekomendasi.this, "Sukses", Toast.LENGTH_LONG).show();
                               Intent intent = new Intent(FormRekomendasi.this, DetailTripSayaActivity.class);
                               startActivity(intent);
                           }
                       });
                   }
               });
            }
        });


    }

    public void SelectImage(View view){
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_IMAGE );
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data ){
        super.onActivityResult(requestCode, resultCode,data);
        if (resultCode== Activity.RESULT_OK&& requestCode ==  PICK_IMAGE){
            imgUrl=data.getData();
            imageUri.setImageURI(imgUrl);

        }
    }

    private String getFileExtension(Uri uri){
        ContentResolver cr = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cr.getType(uri));
    }

}
