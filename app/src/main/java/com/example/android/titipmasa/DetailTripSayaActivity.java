package com.example.android.titipmasa;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import id.titipmasa.adapter.RekomendasiBarangAdapter;
import id.titipmasa.model.RekomendasiBarang;

public class DetailTripSayaActivity extends AppCompatActivity {

    ArrayList<RekomendasiBarang> rekomendasiBarangs = new ArrayList<>();
    RecyclerView recyclerView;
    DatabaseReference databaseReference;
    FirebaseAuth auth;
    StorageReference storageReference;
    String id_traveller;
    RekomendasiBarangAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_trip_saya);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Daftar Barang Rekomendasi");

        recyclerView = findViewById(R.id.rv_list_rekom);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        databaseReference = FirebaseDatabase.getInstance().getReference();
        Query query = databaseReference.child("RekomendasiBarang").orderByChild("id_tarveller").equalTo(id_traveller);

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){
                    RekomendasiBarang rekom = dataSnapshot1.getValue(RekomendasiBarang.class);

                    rekomendasiBarangs.add(rekom);
                }
                adapter = new RekomendasiBarangAdapter(DetailTripSayaActivity.this, rekomendasiBarangs);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DetailTripSayaActivity.this, FormRekomendasi.class);
                startActivity(intent);
            }
        });
    }

}
