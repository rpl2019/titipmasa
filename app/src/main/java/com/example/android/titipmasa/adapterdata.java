package com.example.android.titipmasa;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class adapterdata extends RecyclerView.Adapter<adapterdata.viewHolder>{
    ArrayList<polaItem> listData = new ArrayList<>();
    private adapterdata.onclick listener;

    interface onclick{
        void diklik(polaItem saatIni);
    }

    public adapterdata(ArrayList<polaItem> listData, adapterdata.onclick listener) {
        this.listData = listData;
        this.listener = listener;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new viewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder viewHolder, final int position) {
        viewHolder.barang.setText(listData.get(position).getNama());
        viewHolder.harga.setText(listData.get(position).getharga());
        if (Objects.equals(listData.get(position).getKota(), "Aceh")){
            viewHolder.gambarkota.setImageResource(R.drawable.aceh);
        }else if (Objects.equals(listData.get(position).getKota(), "Bali")){
            viewHolder.gambarkota.setImageResource(R.drawable.bali);
        }

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { listener.diklik(listData.get(position)); }
        });
    }

    public  void addItem(polaItem baru){
        listData.add(baru);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() { return listData.size(); }

    class viewHolder extends RecyclerView.ViewHolder{
        ImageView gambarkota;
        TextView barang, harga, kota;
        public viewHolder(@NonNull View itemView) {
            super(itemView);
            gambarkota = itemView.findViewById(R.id.avatar);
            barang = itemView.findViewById(R.id.txtNama);
            harga = itemView.findViewById(R.id.txtHarga);
            kota = itemView.findViewById(R.id.txtKota);

        }
    }
}
