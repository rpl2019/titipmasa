package com.example.android.titipmasa.model;

public class User {

    String nama, email;

    public User(String nama, String email) {
        this.nama = nama;
        this.email = email;
    }

    public String getNama() {
        return nama;
    }

    public String getEmail() {
        return email;
    }


}
