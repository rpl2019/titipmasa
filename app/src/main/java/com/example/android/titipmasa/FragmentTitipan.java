package com.example.android.titipmasa;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import id.titipmasa.adapter.TitipBarangAdapter;
import id.titipmasa.model.TitipBarang;

public class FragmentTitipan extends Fragment {

    TextView nama_penitip, harga_barang, nama_barang;
    RecyclerView recyclerView;
    DatabaseReference mDatabaseRef;
    StorageReference mStorageRef;
    TitipBarangAdapter adapter;
    String id_traveller;

    ArrayList<TitipBarang> listTitipan = new ArrayList<>();

    public FragmentTitipan() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment.
        View view = inflater.inflate(R.layout.list_titipan_barang, container, false);
        nama_penitip = view.findViewById(R.id.tv_nama_penitip);
        harga_barang = view.findViewById(R.id.tv_harga_titipan);
        nama_barang = view.findViewById(R.id.tv_nama_barang_titipan);

        mDatabaseRef = FirebaseDatabase.getInstance().getReference();

        recyclerView = view.findViewById(R.id.rv_list_titipan_barang);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        return  view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
         id_traveller = getActivity().getIntent().getStringExtra("USER_ID");

         mDatabaseRef = FirebaseDatabase.getInstance().getReference();

        Query query = mDatabaseRef.child("TitipBarang").orderByChild("id_traveller").equalTo(id_traveller);
        Log.d("traveller", "onCreateView: "+id_traveller);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){

                    TitipBarang titip = dataSnapshot1.getValue(TitipBarang.class);

                    listTitipan.add(titip);

                }
                adapter = new TitipBarangAdapter(getContext(), listTitipan);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        Log.d("idtraveller", "onActivityCreated: "+ id_traveller);
    }
}
