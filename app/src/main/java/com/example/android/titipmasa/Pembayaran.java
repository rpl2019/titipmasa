package com.example.android.titipmasa;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;

public class Pembayaran extends AppCompatActivity {
    AlertDialog.Builder dialog;
    LayoutInflater inflatert;
    View dialogView;
    DatePickerDialog dpd;
    TimePickerDialog tpd;
    TextView masukansaldo;
    TextView jumlahsaldo;
    TextView pilihtanggal,pilihwaktu,pilihtanggalpulang,pilihwaktupulang;
    Switch pp;
    Button btnbayar;
    Boolean check_pp = false;
    private static int topup = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pembayaran);
        masukansaldo = findViewById(R.id.masukansaldo);
        btnbayar = findViewById(R.id.btnbayar);
        jumlahsaldo = findViewById(R.id.jumlahsaldo);
        pilihtanggal = findViewById(R.id.pilihtanggal);
        pilihtanggalpulang = findViewById(R.id.pilihtanggalpulang);
        pilihwaktu = findViewById(R.id.pilihwaktu);
        pilihwaktupulang = findViewById(R.id.pilihwaktupulang);


        if (getIntent().getIntExtra("saldo", 0)!=0){
            topup = getIntent().getIntExtra("saldo", 0);
            jumlahsaldo.setText("Rp. "+NumberFormat.getNumberInstance(Locale.US).format(topup));
        }

        pp = findViewById(R.id.pp);
        pp.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    check_pp = true;
                    ((LinearLayout)findViewById(R.id.buatpulang)).setVisibility(View.VISIBLE);
                }else {
                    check_pp = false;
                    ((LinearLayout)findViewById(R.id.buatpulang)).setVisibility(View.GONE);
                }
            }
        });

        masukansaldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogForm();
            }
        });


        btnbayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((EditText)findViewById(R.id.jumlahtitipan)).getText().length()==0){
                    ((EditText) findViewById(R.id.jumlahtitipan)).setError("Masukkan jumlah titipan!");
                }else {
                    int harga = 0;
                    String tuju = ((Spinner)findViewById(R.id.tujuan)).getSelectedItem().toString();
                    String [] tujuans = tuju.split("-");
                    switch (((Spinner)findViewById(R.id.tujuan)).getSelectedItem().toString()){
                        case "Jakarta-(Rp50.000)": harga = 50000;
                            break;
                        case "Aceh-(Rp70.000)": harga = 70000;
                            break;
                        case "Surabaya-(Rp55.000)": harga = 55000;
                            break;
                    }

                    harga=harga*Integer.valueOf(((EditText)findViewById(R.id.jumlahtitipan)).getText().toString());

                    if (check_pp){
                        harga=harga*2;
                    }

                    if (harga>topup){
                        Toast.makeText(Pembayaran.this, "Saldo gak cukup", Toast.LENGTH_SHORT).show();
                        DialogForm();
                    }else {
                        Intent intent = new Intent(Pembayaran.this, Checkout.class);
                        intent.putExtra("topup", topup);
                        intent.putExtra("harga", harga);
                        intent.putExtra("tujuan", tujuans[0]);
                        intent.putExtra("tglbrgkt", pilihtanggal.getText().toString()+" - "+pilihwaktu.getText().toString());
                        intent.putExtra("jumlahtitipan", ((EditText)findViewById(R.id.jumlahtitipan)).getText().toString());
                        intent.putExtra("check_pp", check_pp);
                        intent.putExtra("tglplg",pilihtanggalpulang.getText().toString()+" - "+pilihwaktupulang.getText().toString());
                        Pembayaran.this.startActivity(intent);
                    }
                }
            }
        });
    }

    private void DialogForm() {
        dialog = new AlertDialog.Builder(Pembayaran.this);
        inflatert = getLayoutInflater();
        dialogView = inflatert.inflate(R.layout.topup, null);
        dialog.setView(dialogView);
        dialog.setCancelable(true);
        dialog.setIcon(R.mipmap.ic_launcher);
        dialog.setTitle("masukan jumlah saldo");

        masukansaldo = (EditText) dialogView.findViewById(R.id.masukansaldo);

        dialog.setPositiveButton("TAMBAH SALDO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                TextView jumlahsaldo = (TextView) findViewById(R.id.jumlahsaldo);
                // String saldoAwal = jumlahsaldo.getText().toString();
                Toast.makeText(Pembayaran.this, "Top sejumlah Rp."+masukansaldo.getText().toString()+" berhasil!", Toast.LENGTH_SHORT).show();
                topup = topup+Integer.valueOf(masukansaldo.getText().toString());
                jumlahsaldo.setText("Rp."+NumberFormat.getNumberInstance(Locale.US).format(topup));
                dialog.dismiss();
            }
        });
        dialog.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog.show();
//        };

    }

    public void setTime(final View view) {
        tpd = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int i, int i1) {
                if (view.getId()==R.id.pilihwaktu){
                    pilihwaktu.setText(i+":"+i1);
                }else {
                    pilihwaktupulang.setText(i+":"+i1);
                }
                tpd.dismiss();
            }
        }, Calendar.getInstance().get(Calendar.HOUR), Calendar.getInstance().get(Calendar.MINUTE), true);

        tpd.show();
    }

    public void setDate(final View view) {

        dpd = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                if (view.getId()==R.id.pilihtanggal){
                    pilihtanggal.setText(i2+"/"+(i1+1)+"/"+i);
                }else {
                    pilihtanggalpulang.setText(i2+"/"+(i1+1)+"/"+i);
                }
                dpd.dismiss();
            }
        }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        dpd.show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(Pembayaran.class.getSimpleName(),"onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(Pembayaran.class.getSimpleName(),"onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(Pembayaran.class.getSimpleName(), "onPause");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(Pembayaran.class.getSimpleName(), "onDestroy");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(Pembayaran.class.getSimpleName(), "onStop");
    }
}

