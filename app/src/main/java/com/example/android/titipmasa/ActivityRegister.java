package com.example.android.titipmasa;

//import android.app.ProgressDialog;
//import android.content.Intent;
//import android.graphics.Color;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.text.Editable;
//import android.text.TextWatcher;
//import android.util.Log;
//import android.view.View;
//import android.view.WindowManager;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.TextView;
//import android.widget.Toast;

//import com.google.android.material.textfield.TextInputLayout;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;

//
public class ActivityRegister extends AppCompatActivity {
    ManagerSession managerSession;
    Button btnRegister;

    TextInputLayout input_layout_name, input_layout_email, input_layout_password, input_layout_repassword;
    EditText inName, inEmail, inPassword, inRepassword;
    TextView btnLogin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        getWindow().setStatusBarColor(Color.TRANSPARENT);
        setContentView(R.layout.activity_register);

        managerSession = new ManagerSession(ActivityRegister.this);

        /**
         * Layout variable declaration start here
         */
        input_layout_name      = findViewById(R.id.input_layout_name);
        input_layout_email     = findViewById(R.id.input_layout_email);
        input_layout_password   = findViewById(R.id.input_layout_password);
        input_layout_repassword = findViewById(R.id.input_layout_repassword);
        inName       = findViewById(R.id.inName);
        inEmail      = findViewById(R.id.inEmail);
        inPassword   = findViewById(R.id.inPassword);
        inRepassword = findViewById(R.id.inRepassword);
        btnRegister  = findViewById(R.id.btnRegister);
        btnLogin     = findViewById(R.id.btnLogin);
        /**
         * Layout variables declaration end here
         */


        /*if(managerSession.isLoggedIn()){
            startActivity(new Intent(ActivityRegister.this, ActivityMain.class));
        }*/

        inName.addTextChangedListener(new RegisterTextWatcher(inName));
        inEmail.addTextChangedListener(new RegisterTextWatcher(inEmail));
        inPassword.addTextChangedListener(new RegisterTextWatcher(inPassword));
        inRepassword.addTextChangedListener(new RegisterTextWatcher(inRepassword));

        btnRegisterListener();
        btnLoginListener();
    }

    public void btnLoginListener(){
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ActivityRegister.this, ActivityMain.class));
                finish();
            }
        });
    }

    public void btnRegisterListener(){
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!validateName() || !validateEmail() || !validatePassword() || !validateRepassword()){
                    return;
                }
                new HttpActivityRegister(inName.getText().toString(), inEmail.getText().toString(), inPassword.getText().toString()).execute();
            }
        });
    }

    public class HttpActivityRegister extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;
        private volatile boolean running = true;

        private final static String URL = "https://app.titipmasa.id/public/api/userregister";
        protected String name, email, password, status, message, token;
        JSONObject jsonObject, jsonErrors;
        JSONParser jsonParser;
        JSONArray  jsonArray;

        public HttpActivityRegister(String name, String email, String password){
            this.name     = name;
            this.email    = email;
            this.password = password;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ActivityRegister.this);
            progressDialog.setMessage("Signing up...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            jsonParser = new JSONParser();

            HashMap<String, String> params = new HashMap<>();
            params.put("name", name);
            params.put("email", email);
            params.put("password", password);

            try{
                jsonObject = jsonParser.makeHttpRequest(URL, "POST", params, "");
                status = jsonObject.getString("status");

                if(!jsonObject.getString("errors").isEmpty()){
                    jsonErrors = jsonObject.getJSONObject("errors");
                    Log.d("JSON Errors", String.valueOf(jsonErrors));
                    jsonArray = jsonErrors.getJSONArray("email");
                    for(int i = 0; i < jsonArray.length(); i++){
                        message = jsonArray.getString(i);
                    }
                }

                if(!jsonObject.getString("token").isEmpty()){
                    token = jsonObject.getString("token");
                }

            }catch (Exception ex){
                ex.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if(progressDialog != null && progressDialog.isShowing()){
                progressDialog.dismiss();
            }

            if(status.equals("success")){
                managerSession.createLoginSession(email, token);
                Toast.makeText(ActivityRegister.this, "Sign up success", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(ActivityRegister.this, message, Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
    }

    private class RegisterTextWatcher implements TextWatcher {

        private View view;

        private RegisterTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.inName:
                    validateName();
                    break;
                case R.id.inPassword:
                    validatePassword();
                    break;
                case R.id.inRepassword:
                    validateRepassword();
                    break;
                case R.id.inEmail:
                    validateEmail();
                    break;
            }
        }
    }

    private Boolean validateName(){
        if (inName.getText().toString().trim().isEmpty()){
            input_layout_name.setError("Isi nama Anda.");
            requestFocus(inName);
            return false;
        }else if(inName.getText().toString().length() > 150){
            input_layout_name.setError("Nama lebih dari 150 karakter.");
            requestFocus(inName);
            return false;
        }else{
            input_layout_name.setErrorEnabled(false);
        }
        return true;
    }

    private Boolean validateEmail(){
        if (inEmail.getText().toString().trim().isEmpty()){
            input_layout_email.setError("Isi password Anda");
            requestFocus(inEmail);
            return false;
        }else if(inEmail.getText().toString().length() > 255){
            input_layout_email.setError("Password lebih dari 255 karakter");
            requestFocus(inEmail);
            return false;
        }else{
            input_layout_email.setErrorEnabled(false);
        }
        return true;
    }

    private Boolean validatePassword(){
        if (inPassword.getText().toString().trim().isEmpty()){
            input_layout_password.setError("Isi password Anda");
            requestFocus(inPassword);
            return false;
        }else if(inPassword.getText().toString().length() > 50){
            input_layout_password.setError("Password lebih dari 50 karakter");
            requestFocus(inPassword);
            return false;
        }else{
            input_layout_password.setErrorEnabled(false);
        }
        return true;
    }

    private Boolean validateRepassword(){
        if (inRepassword.getText().toString().trim().isEmpty()){
            input_layout_repassword.setError("Isi password konfirmasi Anda");
            requestFocus(inRepassword);
            return false;
        }else if(!inRepassword.getText().toString().equals(inPassword.getText().toString())){
            input_layout_repassword.setError("Password tidak sama");
            requestFocus(inRepassword);
            return false;
        }else{
            input_layout_repassword.setErrorEnabled(false);
        }
        return true;
    }

    private void requestFocus(View view){
        if(view.requestFocus()){
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
}
