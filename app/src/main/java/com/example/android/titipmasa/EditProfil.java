package com.example.android.titipmasa;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import androidx.appcompat.app.AppCompatActivity;
import id.titipmasa.model.User;

public class EditProfil extends AppCompatActivity {

    EditText edtNama, edtALamat, edtNoHP, edtNIK;
    ImageView imgProfil, imgKTP;
    Button simpan;
    FirebaseAuth auth;
    DatabaseReference mDatabaseRef;
    StorageReference mStorareref;
    Uri imageUriProfil, imageUriKTP;
    private static final int PICK_IMAGE = 100;
    private ProgressDialog progressDialog;
    public String urlProfil, urlKTP;
    String id_user;
    String urlImageKtp, urlImageProfil;
    int profil = 0;
    View view;
    StorageReference imageRefProfil, imageRefKTP;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profil);

        edtNama = findViewById(R.id.edtEditNama);
        edtALamat = findViewById(R.id.edtEditAlamat);
        edtNoHP = findViewById(R.id.edtEditNoHP);
        edtNIK = findViewById(R.id.edtEditNIK);
        imgKTP = findViewById(R.id.img_ktp);
        imgProfil = findViewById(R.id.img_profil);
        progressDialog = new ProgressDialog(this);
        mDatabaseRef = FirebaseDatabase.getInstance().getReference("users");
        final String user_id = FirebaseAuth.getInstance().getCurrentUser().getUid();
        mStorareref = FirebaseStorage.getInstance().getReference("users");

        simpan = findViewById(R.id.btn_simpan);


        //get intent
        String namaProfil = getIntent().getStringExtra("NAMA");
        String alamatProfil = getIntent().getStringExtra("ALAMAT");
        String noHp = getIntent().getStringExtra("NOHP");
        String nik = getIntent().getStringExtra("NIK");
         urlImageKtp = getIntent().getStringExtra("URLIMAGE");
        urlImageProfil = getIntent().getStringExtra("UrlImageProfil");

        edtNama.setText(namaProfil);
        edtALamat.setText(alamatProfil);
        edtNIK.setText(nik);
        edtNoHP.setText(noHp);

        Picasso.get()
                .load(urlImageKtp)
                .into(imgKTP);

        Picasso.get()
                .load(urlImageProfil)
                .into(imgProfil);

        simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.setMessage("Proses...");
                progressDialog.show();
                final String nama, alamat, noHP, NIK;
                if (urlImageKtp == null && urlImageProfil == null) {

                    imageRefProfil = mStorareref.child(System.currentTimeMillis() + "." + getFileExtension(imageUriProfil));
                    imageRefKTP = mStorareref.child(System.currentTimeMillis() + "." + getFileExtension(imageUriKTP));
                }

                nama = edtNama.getText().toString().trim();
                alamat = edtALamat.getText().toString().trim();
                noHP = edtNoHP.getText().toString().trim();
                NIK = edtNIK.getText().toString().trim();

                if (urlImageKtp == null && urlImageProfil == null) {
                    imageRefProfil.putFile(imageUriProfil).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            imageRefProfil.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    urlProfil = uri.toString().trim();
                                    String id_user = mDatabaseRef.push().getKey();
                                    Log.d("url", "onClick: " + urlKTP + " - " + urlProfil);
                                    User user = new User(user_id, nama, alamat, NIK, noHP, urlProfil, urlKTP);
                                    mDatabaseRef.child(user_id).setValue(user);
                                    progressDialog.dismiss();
                                    Toast.makeText(EditProfil.this, "Update Success", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });
                }
                if (urlImageKtp == null && urlImageProfil == null) {
                    imageRefKTP.putFile(imageUriKTP).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            imageRefKTP.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    urlKTP = uri.toString().trim();

                                    String id_user = mDatabaseRef.push().getKey();
                                    Log.d("url", "onClick: " + urlKTP + " - " + urlProfil);
                                    User user = new User(user_id, nama, alamat, NIK, noHP, urlProfil, urlKTP);
                                    mDatabaseRef.child(user_id).setValue(user);
                                    progressDialog.dismiss();
                                    Toast.makeText(EditProfil.this, "Update Success", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });

                }

                if (urlImageProfil != null || urlImageKtp != null){
                    User user = new User(user_id, nama, alamat, NIK, noHP, urlImageProfil, urlImageKtp);
                    mDatabaseRef.child(user_id).setValue(user);
                    progressDialog.dismiss();
                    Toast.makeText(EditProfil.this, "Update Success", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    public void SelectImageProfil(View view){
        profil = 1;
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_IMAGE);
    }

    public void SelectImageKTP(View view){
        profil = 0;
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data ){
        super.onActivityResult(requestCode, resultCode,data);

        if (resultCode== Activity.RESULT_OK&& requestCode ==  PICK_IMAGE) {
            if (profil == 1) {
                imageUriProfil = data.getData();
                imgProfil.setImageURI(imageUriProfil);
            } else {
                imageUriKTP = data.getData();
                imgKTP.setImageURI(imageUriKTP);
            }
        }
    }



    private String getFileExtension(Uri uri){
        ContentResolver cr = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();

        return mime.getExtensionFromMimeType(cr.getType(uri));
    }



}
