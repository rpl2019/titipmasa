package com.example.android.titipmasa;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import id.titipmasa.model.User;

public class DataPribadiActivity extends AppCompatActivity {

    FirebaseAuth auth;
    DatabaseReference mDatabaseRef;
    StorageReference mStorageRef;

    TextView tvNama, tvAlamat, tvEmail, tvNik, tvNoHP;
    ImageView imgProfil, imgKTP;
    String nama,alamat,nik,noHp,urlImage, urlProfil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_pribadi);

        tvNama = findViewById(R.id.tvNama);
        tvAlamat = findViewById(R.id.tvALamat);
        tvNik = findViewById(R.id.tvNIK);
        tvNoHP = findViewById(R.id.tvNoHP);
        imgKTP = findViewById(R.id.imgKTP);

        final String user_id = FirebaseAuth.getInstance().getCurrentUser().getUid();
        mDatabaseRef = FirebaseDatabase.getInstance().getReference("users").child(user_id);

        mDatabaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

//                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){
                    User user = dataSnapshot.getValue(User.class);
                     nama = user.getNama();
                     alamat = user.getAlamat();
                     nik = user.getNik();
                     noHp = user.getNoHP();
                     urlImage = user.getUrlImageKTP();
                     urlProfil = user.getUrlImageProfil();

                    tvNama.setText(nama);
                    tvAlamat.setText(alamat);
                    tvNik.setText(nik);
                    tvNoHP.setText(noHp);

                Picasso.get()
                        .load(urlImage)
                        .resize(400, 400)
                        .into(imgKTP);
//                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void editProfil(View view){
        Intent intent = new Intent(DataPribadiActivity.this, EditProfil.class);
        intent.putExtra("NAMA", nama);
        intent.putExtra("ALAMAT", alamat);
        intent.putExtra("NIK", nik);
        intent.putExtra("NOHP", noHp);
        intent.putExtra("URLIMAGE", urlImage);
        intent.putExtra("UrlImageProfil", urlProfil);
        startActivity(intent);
    }
}
