package com.example.android.titipmasa.model;

public class Pesan {

    String isi_pesan;
    String from_nama;
    String id;
    String to_nama;

    public Pesan(){

    }
    public Pesan(String id, String isi_pesan, String from_nama, String to_nama) {
        this.isi_pesan = isi_pesan;
        this.from_nama = from_nama;
        this.id = id;
        this.to_nama = to_nama;
    }

    public String getIsi_pesan() {
        return isi_pesan;
    }

    public String getFrom_nama() {
        return from_nama;
    }

    public String getId(){
        return id;
    }

    public String getTo_nama(){
        return to_nama;
    }




}
