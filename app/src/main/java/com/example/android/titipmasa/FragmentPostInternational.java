package com.example.android.titipmasa;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import id.titipmasa.model.DataTrip;
import id.titipmasa.model.User;

public class FragmentPostInternational extends Fragment {
    EditText nama_traveller, tgl_kembali, lokasi;
    Spinner tujuan;
    DatabaseReference database, dfUsers;
    FirebaseAuth auth;
    DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;
    String namaTraveller, imgProfil, id_user, user_id;

    public FragmentPostInternational() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_post_trip_international, container, false);

        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        tujuan = view.findViewById(R.id.pilih_tujuan_trip_inter);
        lokasi = view.findViewById(R.id.edt_lokasi_pulang_inter);
        tgl_kembali = view.findViewById(R.id.tgl_pulang_edit);
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        Button btn_date = view.findViewById(R.id.btn_date_inter);
        Button btn_post = view.findViewById(R.id.btn_post_trip_inter);

        user_id = FirebaseAuth.getInstance().getCurrentUser().getUid();
        database = FirebaseDatabase.getInstance().getReference("Trips");
        dfUsers = FirebaseDatabase.getInstance().getReference("users").child(user_id);

        dfUsers.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                namaTraveller = user.getNama();
                imgProfil = user.getUrlImageProfil();
                id_user = user.getId_user();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        btn_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateDialog();
            }
        });
        btn_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTrip();
            }
        });
        return view;

    }

    public void addTrip(){
        String nama, tujuan_traveller, lokasi_pulang,tglkemlbali;


        tujuan_traveller = tujuan.getSelectedItem().toString();
        lokasi_pulang = lokasi.getText().toString().trim();
        tglkemlbali = tgl_kembali.getText().toString().trim();

        if (!TextUtils.isEmpty(lokasi_pulang) && !TextUtils.isEmpty(tglkemlbali)) {

            String id = database.push().getKey();
            Log.d("profil", "addTrip: " + imgProfil);
            DataTrip dataTrip = new DataTrip(id,id_user, namaTraveller, tujuan_traveller,tglkemlbali,lokasi_pulang, imgProfil);

            database.child(id).setValue(dataTrip);


            lokasi.setText("");
            tgl_kembali.setText("");

            Toast.makeText(getContext(), "Post Sukses dibuat", Toast.LENGTH_LONG).show();
        } else {
            //if the value is not given displaying a toast
            Toast.makeText(getContext(), "Please enter a Tujuan", Toast.LENGTH_LONG).show();
        }


    }

    private void showDateDialog(){

        Calendar newCalendar = Calendar.getInstance();

        datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                tgl_kembali.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.show();
    }
}
