package com.example.android.titipmasa;


import android.content.Context;
import android.content.SharedPreferences;

public class ManagerPref {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context context;

    private static final String PREF_NAME = "ActivityMain";

    private static final String IS_FIRSTTIME = "FirstTimeLaunch";

    public ManagerPref(Context context){
        this.context = context;
        pref         = context.getSharedPreferences(PREF_NAME, 0);
        editor       = pref.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime){
        editor.putBoolean(IS_FIRSTTIME, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch(){
        return pref.getBoolean(IS_FIRSTTIME, true);
    }
}
