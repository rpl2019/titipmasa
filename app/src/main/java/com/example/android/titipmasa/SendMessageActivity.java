package com.example.android.titipmasa;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import id.titipmasa.model.TitipBarang;
import id.titipmasa.model.User;

public class SendMessageActivity extends AppCompatActivity {

    EditText edtNama, edtJumlah, edtHarga, edtTujuan;
    ImageView imgBarang;
    Button btnSubmit;
    DatabaseReference mDatabase, dbUsers;
    FirebaseAuth auth;
    FirebaseStorage mStorage;
    StorageReference StorageRef;
    String nama_barang,jumlah_barang,harga_barang,tujuan_pengiriman_barang;
    private static final int PICK_IMAGE = 100;
    Uri imageUri;
    String id_titipan;
    String namaPenitip;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_message);
        setTitle("Form Titip Barang");
        edtNama = findViewById(R.id.edtNamaBarang);
        edtJumlah = findViewById(R.id.edtJumlahBarang);
        edtHarga = findViewById(R.id.edtHargaBarang);
        edtTujuan = findViewById(R.id.edtTujuanPengirimanBarang);
        imgBarang = findViewById(R.id.addImgBarang);
        btnSubmit = findViewById(R.id.btn_submit);

        final String id_traveller =  getIntent().getStringExtra("ID_TRAVELLER");

        Log.d("idtraveller", "onCreate: " + id_traveller);
        mDatabase = FirebaseDatabase.getInstance().getReference("TitipBarang");
        StorageRef = FirebaseStorage.getInstance().getReference("TitipBarang");



        auth = FirebaseAuth.getInstance();
        final String id_user = auth.getCurrentUser().getUid();
        dbUsers = FirebaseDatabase.getInstance().getReference("users").child(id_user);

        dbUsers.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                namaPenitip = user.getNama();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        progressDialog = new ProgressDialog(this);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.setMessage("Proses...");
                progressDialog.show();
                final StorageReference imageRef = StorageRef.child(System.currentTimeMillis()+"."+getFileExtension(imageUri));
                nama_barang = edtNama.getText().toString().trim();
                jumlah_barang = edtJumlah.getText().toString().trim();
                harga_barang = edtHarga.getText().toString().trim();
                tujuan_pengiriman_barang = edtTujuan.getText().toString().trim();


                imageRef.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                Log.d("Upload Foto","onSuccess : uri "+uri.toString());
                                String urlImage = uri.toString().trim() ;
                                id_titipan = mDatabase.push().getKey();
                                Log.d("penitip", "onSuccess: "+ namaPenitip);
                                TitipBarang titipBarang = new TitipBarang(id_titipan, id_user,  id_traveller,namaPenitip, nama_barang, jumlah_barang, harga_barang, tujuan_pengiriman_barang, urlImage);
                                mDatabase.child(id_titipan).setValue(titipBarang);;
                                progressDialog.dismiss();
                                Toast.makeText(SendMessageActivity.this, "Success", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        });
                        // Get a URL to the uploaded content


                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                    }
                });


            }
        });





    }

    public void SelectImage(View view){
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_IMAGE );
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data ){
        super.onActivityResult(requestCode, resultCode,data);
        if (resultCode== Activity.RESULT_OK&& requestCode ==  PICK_IMAGE){
            imageUri=data.getData();
            imgBarang.setImageURI(imageUri);
        }
    }

    private String getFileExtension(Uri uri){
        ContentResolver cr = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cr.getType(uri));
    }


}
