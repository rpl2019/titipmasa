package com.example.android.titipmasa;



import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;

public class Checkout extends AppCompatActivity {

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        ((TextView)findViewById(R.id.tv)).setText("Rp. "+NumberFormat.getNumberInstance(Locale.US).format(getIntent().getIntExtra("harga", 0)));
        ((TextView)findViewById(R.id.tj)).setText(getIntent().getStringExtra("tujuan"));
        ((TextView) findViewById(R.id.tglbrgkt)).setText(getIntent().getStringExtra("tglbrgkt"));
        ((TextView)findViewById(R.id.jumlah)).setText(getIntent().getStringExtra("jumlahtitipan"));

        if (getIntent().getBooleanExtra("check_pp", false)){
            ((LinearLayout)findViewById(R.id.cektanggalpulang)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tglplg)).setText(getIntent().getStringExtra("tglplg"));
        }
    }


    public void klik(View view) {
        int saldo = getIntent().getIntExtra("topup", 0) - getIntent().getIntExtra("harga", 0);
        Intent baru = new Intent(this, Pembayaran.class);
        baru.putExtra("saldo", saldo);
        startActivity(baru);
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(Checkout.class.getSimpleName(), "onStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(Checkout.class.getSimpleName(), "onStop");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(Checkout.class.getSimpleName(), "onResume");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(Checkout.class.getSimpleName(), "onDestroy");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(Checkout.class.getSimpleName(), "onPause");
    }
}
