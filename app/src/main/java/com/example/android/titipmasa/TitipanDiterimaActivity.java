package com.example.android.titipmasa;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import id.titipmasa.adapter.TitipBarangAdapter;
import id.titipmasa.model.TitipBarang;

public class TitipanDiterimaActivity extends AppCompatActivity {

    TextView nama_penitip, harga_barang, nama_barang;
    RecyclerView recyclerView;
    DatabaseReference mDatabaseRef;
    StorageReference mStorageRef;
    TitipBarangAdapter adapter;
    String user_id;
    FirebaseAuth auth;

    ArrayList<TitipBarang> listTitipan = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_titipan_barang);
        setTitle("Daftar Titipan Masuk");

        nama_penitip = findViewById(R.id.tv_nama_penitip);
        harga_barang = findViewById(R.id.tv_harga_titipan);
        nama_barang = findViewById(R.id.tv_nama_barang_titipan);

        mDatabaseRef = FirebaseDatabase.getInstance().getReference();

        recyclerView = findViewById(R.id.rv_list_titipan_barang);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);


        user_id = FirebaseAuth.getInstance().getCurrentUser().getUid();

        mDatabaseRef = FirebaseDatabase.getInstance().getReference();

        Query query = mDatabaseRef.child("TitipBarang").orderByChild("user_id").equalTo(user_id);

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){

                    TitipBarang titip = dataSnapshot1.getValue(TitipBarang.class);

                    listTitipan.add(titip);

                }
                adapter = new TitipBarangAdapter(TitipanDiterimaActivity.this, listTitipan);
                adapter.notifyDataSetChanged();
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    public void DetailTitipan(View view){
        Intent intent = new Intent(TitipanDiterimaActivity.this, DetailTitipanSaya.class);
        startActivity(intent);
    }
}
