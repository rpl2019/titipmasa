package com.example.android.titipmasa;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

    private ArrayList<polaItem> listUser;
    private Context mContext;

    public ListAdapter(ArrayList<polaItem> listUser, Context mContext) {
        this.listUser = listUser;
        this.mContext = mContext;
    }



    @NonNull
    @Override
    public ListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.list_item, viewGroup, false ));
    }

    @Override
    public void onBindViewHolder(@NonNull ListAdapter.ViewHolder viewHolder, int posisi) {

        polaItem user = listUser.get(posisi);
        viewHolder.bindTo(user);

    }

    @Override
    public int getItemCount() {
        return listUser.size();
    }

    class ViewHolder extends  RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView nama,job;

        private ImageView foto;
        private int avatarCode;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            nama = itemView.findViewById(R.id.txtNama);
            job = itemView.findViewById(R.id.txtHarga);
            foto = itemView.findViewById(R.id.avatar);

            itemView.setOnClickListener(this);

        }

        public void bindTo(polaItem user) {
            nama.setText(user.getNama());
            job.setText(user.getharga());

            avatarCode = user.getAvatar();
            switch (user.getAvatar()){
                case 1:
                    foto.setImageResource(R.drawable.baju);
                    break;
                case 2:
                    foto.setImageResource(R.drawable.bolulembang);
                    break;
                case 3:
                    foto.setImageResource(R.drawable.celana);
                    break;
                case 4:
                    foto.setImageResource(R.drawable.jamtangan);
                    break;
                case 5:
                    foto.setImageResource(R.drawable.kopitoraja);
                    break;
                case 6:
                    foto.setImageResource(R.drawable.kue);
                    break;
                case 7:
                default:
                    foto.setImageResource(R.drawable.miecoto);
                    break;
            }
        }

        @Override
        public void onClick(View v) {
            Intent toDetailActivity = new Intent(v.getContext(),ActivityDetail.class);
            toDetailActivity.putExtra("nama",nama.getText().toString());
            toDetailActivity.putExtra("gender",avatarCode);
            toDetailActivity.putExtra("job",job.getText().toString());
            v.getContext().startActivity(toDetailActivity);
        }
    }
}
