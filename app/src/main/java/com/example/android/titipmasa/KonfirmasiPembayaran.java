package com.example.android.titipmasa;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.Calendar;


public class KonfirmasiPembayaran extends AppCompatActivity {

    DatePickerDialog date;
    EditText tgl;
    ImageView image;
    String imagelocation;
    Uri filePath;
    Button btnFoto, btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_konfirmasi_pembayaran);

        image=(ImageView)findViewById(R.id.image);
        btnSubmit=(Button)findViewById(R.id.btnSubmit);
        tgl=findViewById(R.id.tgl);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(KonfirmasiPembayaran.this, ActivityHome.class));

            }
        });

    }
    public void setDate(final View view) {
        date = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                if (view.getId()==R.id.tgl){
                    tgl.setText(i2+"/"+(i1+1)+"/"+i);
                }

                date.dismiss();
            }
        }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        date.show();
    }

}
