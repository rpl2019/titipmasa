package com.example.android.titipmasa;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

//import android.support.annotation.NonNull;
//import android.support.design.widget.FloatingActionButton;
////import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.GridLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.support.v7.widget.Toolbar;
//import android.support.v7.widget.helper.ItemTouchHelper;


public class MainActivity2 extends AppCompatActivity {


    Button cancel, submit;
    RecyclerView mRecyclerView;
    Dialog dialog;
    private ArrayList<polaItem> daftarUser;
    private ListAdapter userAdapter;
    ArrayAdapter<String> adapter;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_titip);
//        toolbar = findViewById(R.id.toolbar);
//        toolbar.setNavigationIcon(R.drawable.keranjang_icon);


        mRecyclerView = findViewById(R.id.recycler_view);
        int gridColumnCount =getResources().getInteger(R.integer.grid_column_count);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this,gridColumnCount));

        daftarUser = new ArrayList<>();

        if (savedInstanceState != null){
            daftarUser.clear();

            for (int i= 0; i< savedInstanceState.getStringArrayList("nama").size(); i++){

                daftarUser.add(new polaItem(savedInstanceState.getStringArrayList("nama").get(i),
                        savedInstanceState.getStringArrayList("harga").get(i),
                        Objects.requireNonNull(savedInstanceState.getIntegerArrayList("kota")).get(i)));

            }
        }

        userAdapter = new ListAdapter(daftarUser, this);
        mRecyclerView.setAdapter(userAdapter);

//        userAdapter = new ListAdapter(daftarUser, this);
//        mRecyclerView.setAdapter(userAdapter);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowPopup(view);
            }
        });

        ItemTouchHelper helper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.DOWN | ItemTouchHelper.UP,
               ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT ) {

            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                int dari = viewHolder.getAdapterPosition();
                int ke = target.getAdapterPosition();
                Collections.swap(daftarUser, dari, ke);
                userAdapter.notifyItemMoved(dari,ke);

                return true;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                daftarUser.remove(viewHolder.getAdapterPosition());
                userAdapter.notifyItemRemoved(viewHolder.getAdapterPosition());

            }
        });
        helper.attachToRecyclerView(mRecyclerView);
    }


    public void ShowPopup(View v) {
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.custom_dialog);
        final EditText mNama,mPekerjaan;
        final Spinner mGender;
        mNama = dialog.findViewById(R.id.edt_nama);
        mPekerjaan = dialog.findViewById(R.id.edt_job);

        Button tambah=dialog.findViewById(R.id.btn_submit);
        Button batal = dialog.findViewById(R.id.btn_cancel);

        mGender = dialog.findViewById(R.id.gender);

        String[]list={"Baju","Bolu Lembang", "Celana", "Jam Tangan", "Kopi Toraja", "Kue Artis", "IndoMie Coto Makassar"};

        ArrayAdapter<String>adapterX = new ArrayAdapter(dialog.getContext(),android.R.layout.simple_spinner_item,list);
        mGender.setAdapter(adapterX);

        tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                daftarUser.add(new polaItem(mNama.getText().toString(),mPekerjaan.getText().toString(),mGender.getSelectedItemPosition()+1));
                userAdapter.notifyDataSetChanged();
                dialog.dismiss();
            }
        });
        batal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void onSaveInstanceState(Bundle state) {
        ArrayList<String> tmpNama = new ArrayList<>();
        ArrayList<String> tmpJob = new ArrayList<>();
        ArrayList<Integer> tmpGender = new ArrayList<>();

        for (int i=0; i<daftarUser.size(); i++){
            tmpNama.add(daftarUser.get(i).getNama());
            tmpJob.add(daftarUser.get(i).getharga());
            tmpGender.add(daftarUser.get(i).getAvatar());
        }

        state.putStringArrayList("nama", tmpNama);
        state.putStringArrayList("job", tmpJob);
        state.putIntegerArrayList("gender", tmpGender);

        super.onSaveInstanceState(state);
    }



}
