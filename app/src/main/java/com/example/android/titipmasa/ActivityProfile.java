package com.example.android.titipmasa;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ActivityProfile extends AppCompatActivity {
    ManagerSession managerSession;
    ImageView imageProfile;
    TextView textName, textOrder, textWishlist, textCart, textEmail, textAddress, textPhone, textGiftCard;
    View viewTripSayaProfil, viewDataPribadiProfil;
    Button btnLogout;

    public static final String KEY_TOKEN    = "Token";
    public static final String KEY_USERMAIL = "UserEmail";
    private static final String PREF_NAME   = "LoginSession"; //Session Name

    String user_token, user_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);


        managerSession = new ManagerSession(ActivityProfile.this);

        SharedPreferences sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        user_token = sharedPreferences.getString(KEY_TOKEN, null);
        user_email = sharedPreferences.getString(KEY_USERMAIL, null);


    }

    public void tripSaya(View view){
        viewTripSayaProfil = (View)findViewById(R.id.view_trip_saya_profil);
         Intent intent = new Intent(ActivityProfile.this, TripSayaActivity.class);
         startActivity(intent);
    }

    public void dataPribadi(View view){
        viewDataPribadiProfil = (View)findViewById(R.id.view_data_pribadi_profil);
        Intent intent = new Intent(ActivityProfile.this, DataPribadiActivity.class);
        startActivity(intent);
    }

//    public void btnLogoutListener(){
////        btnLogout.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View view) {
////
////            }
////        });
////    }
////
////    public class HttpActivityProfile extends AsyncTask<String, String, String>{
////        ProgressDialog progressDialog;
////        private volatile boolean running = true;
////
////        private final static String URL = "https://app.titipmasa.id/public/api/user";
////        protected String name, verified_email, foto, alamat, phone, status, message, token;
////        JSONObject jsonObject;
////        JSONParser jsonParser;
////
////        public HttpActivityProfile(String token) {
////            this.token = token;
////        }
////
////        @Override
////        protected void onPreExecute() {
////            super.onPreExecute();
////            progressDialog = new ProgressDialog(ActivityProfile.this);
////            progressDialog.setMessage("Getting your profile...");
////            progressDialog.setIndeterminate(false);
////            progressDialog.setCancelable(true);
////            progressDialog.setCanceledOnTouchOutside(false);
////            progressDialog.show();
////        }
////
////        @Override
////        protected String doInBackground(String... strings) {
////            jsonParser = new JSONParser();
////
////            HashMap<String, String> params = new HashMap<>();
////            try {
////                jsonObject = jsonParser.makeHttpRequest(URL, "GET", params, token);
////                name = jsonObject.getString("name");
////                if(jsonObject.getString("email_verified_at") != null){
////                    verified_email = jsonObject.getString("email_verified_at");
////                }
////
////                verified_email = "Belum diisi";
////
////                if(jsonObject.getString("alamat") != null){
////                    alamat = jsonObject.getString("alamat");
////                }
////
////                alamat = "Belum diisi";
////
////                foto = jsonObject.getString("foto");
////
////                if(jsonObject.getString("phone") != null){
////                    phone = jsonObject.getString("phone");
////                }
////
////                phone = "Belum diisi";
////
////            }catch(JSONException ex){
////                ex.printStackTrace();
////            }
////            return null;
////        }
////
////        @Override
////        protected void onPostExecute(String s) {
////            if(progressDialog != null && progressDialog.isShowing()){
////                progressDialog.dismiss();
////            }
////
////            textName.setText(name);
////            textAddress.setText(alamat);
////            textEmail.setText(user_email);
////            textPhone.setText(phone);
////        }
////
////        @Override
////        protected void onCancelled() {
////            super.onCancelled();
////        }
////    }
}
