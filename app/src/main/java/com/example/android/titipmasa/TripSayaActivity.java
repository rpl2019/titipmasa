package com.example.android.titipmasa;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import id.titipmasa.adapter.TripSayaAdapter;
import id.titipmasa.model.DataTrip;

public class TripSayaActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ArrayList<DataTrip> trips = new ArrayList<>();
    TripSayaAdapter adapter;
    FirebaseAuth auth;
    DatabaseReference mDatabseRef;
    Button btnEdit, btnDelete;
    String id;
    String id_user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_trip_saya);
        setTitle("Daftar Trip");

        recyclerView = findViewById(R.id.rv_list_trip_saya);
        btnEdit = findViewById(R.id.btn_edit_trip);
        btnDelete = findViewById(R.id.btn_delete_trip);
        id_user = FirebaseAuth.getInstance().getCurrentUser().getUid();
        mDatabseRef = FirebaseDatabase.getInstance().getReference();

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        disyplay();

    }



    public void detail_trip(View view){
        Intent intent = new Intent(TripSayaActivity.this, DetailTripSayaActivity.class);
        startActivity(intent);
    }

    public void disyplay(){
        Query query = mDatabseRef.child("Trips").orderByChild("user_id").equalTo(id_user);

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){
                    DataTrip trip = dataSnapshot1.getValue(DataTrip.class);
                    id = trip.getId();
                    trips.add(trip);
                }

                adapter = new TripSayaAdapter(TripSayaActivity.this, trips);
                adapter.notifyDataSetChanged();
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {


            }
        });
    }
}
