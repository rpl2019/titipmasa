package com.example.android.titipmasa;


import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ActivityDetail extends AppCompatActivity {

    TextView detailNama, detailHarga;
    ImageView foto;
    String mNama, mHarga;
    int avatar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_user);

        detailNama = findViewById(R.id.txt_nama);
        detailHarga = findViewById(R.id.txt_harga);

        foto = findViewById(R.id.detail_foto);

        mNama = getIntent().getStringExtra("nama");
        mHarga = getIntent().getStringExtra("harga");
        avatar = getIntent().getIntExtra("gender", 2);


        detailNama.setText(mNama);
        detailHarga.setText(mHarga);

        switch (avatar){
            case 1:
                foto.setImageResource(R.drawable.baju);
                break;
            case 2:
                foto.setImageResource(R.drawable.bolulembang);
                break;
            case 3:
                foto.setImageResource(R.drawable.celana);
                break;
            case 4:
                foto.setImageResource(R.drawable.jamtangan);
                break;
            case 5:
                foto.setImageResource(R.drawable.kopitoraja);
                break;
            case 6:
                foto.setImageResource(R.drawable.kue);
                break;
            case 7:
            default:
                foto.setImageResource(R.drawable.miecoto);
                break;

        }
    }
}