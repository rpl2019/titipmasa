package com.example.android.titipmasa;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import id.titipmasa.adapter.PesanMasukAdapter;
import id.titipmasa.model.Pesan;

public class PesanActivity extends Fragment {

    RecyclerView recyclerView;
    TextView tvNama, tvIsipesan;
    FirebaseAuth auth;
    DatabaseReference mDatabaseRef, userRef;
    StorageReference mStorageRef;
    ArrayList<Pesan> listPesan = new ArrayList<>();
    PesanMasukAdapter adapter;
    String imgProfilPenerima;
    String toName;String id_user;
    public PesanActivity() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.list_pesan, container, false);

        recyclerView = view.findViewById(R.id.rv_list_pesan);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
         id_user = FirebaseAuth.getInstance().getCurrentUser().getUid();
        mDatabaseRef = FirebaseDatabase.getInstance().getReference();


        Query query = mDatabaseRef.child("Message").orderByChild("to_user_id").equalTo(id_user).limitToLast(1);

        Query query1 = mDatabaseRef.child("Message").orderByChild("user_id").equalTo(id_user).limitToLast(1);

        if (query1 != null){
            query1.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){
                        Pesan pesan = dataSnapshot1.getValue(Pesan.class);

                        listPesan.add(pesan);
                    }

                    adapter = new PesanMasukAdapter(getContext(), listPesan);
                    adapter.notifyDataSetChanged();

                    recyclerView.setAdapter(adapter);
//                adapter.notifyDataSetChanged();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        } else {
            query.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){
                        Pesan pesan = dataSnapshot1.getValue(Pesan.class);

                        listPesan.add(pesan);
                    }

                    adapter = new PesanMasukAdapter(getContext(), listPesan);
                    adapter.notifyDataSetChanged();

                    recyclerView.setAdapter(adapter);
//                adapter.notifyDataSetChanged();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }

        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }


}
