package com.example.android.titipmasa;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;
import com.github.paolorotolo.appintro.model.SliderPage;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class ActivityIntro extends AppIntro {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        getWindow().setStatusBarColor(Color.TRANSPARENT);

        SliderPage sliderPage1 = new SliderPage();
        sliderPage1.setTitle("Welcome!");
        sliderPage1.setDescription("Titip mudah dimana saja, kapan saja titipmasa.id");
        sliderPage1.setImageDrawable(R.drawable.slider1);
        sliderPage1.setBgColor(Color.rgb(247, 91, 4));
        addSlide(AppIntroFragment.newInstance(sliderPage1));

        SliderPage sliderPage2 = new SliderPage();
        sliderPage2.setTitle("Titip Yuk!");
        sliderPage2.setDescription("Cari, Temukan\r\nTitip belanja atau titip barang dari berbagai daerah/lokasi di Indonesia jadi lebih mudah.");
        sliderPage2.setImageDrawable(R.drawable.slider2);
        sliderPage2.setBgColor(Color.rgb(247, 91, 4));
        addSlide(AppIntroFragment.newInstance(sliderPage2));
        setProgressButtonEnabled(true);
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        startActivity(new Intent(ActivityIntro.this, ActivityMain.class));
        finish();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        startActivity(new Intent(ActivityIntro.this, ActivityMain.class));
        finish();
    }
}