package com.example.android.titipmasa;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import id.titipmasa.model.User;

public class FragmentProfil extends Fragment {
FirebaseAuth auth;
DatabaseReference mDatabaseRef;
ImageView imgProfil;
TextView nama;

DatabaseReference mDatabase;
    public FragmentProfil() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        auth = FirebaseAuth.getInstance();

        String user_id = auth.getCurrentUser().getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference("users").child(user_id);


        View view = inflater.inflate(R.layout.activity_profile, container, false);
        Button btn = view.findViewById(R.id.btn_logout);
        nama = view.findViewById(R.id.tv_name);
        imgProfil = view.findViewById(R.id.img_profile);

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);

                String namaUser = user.getNama();
                String urlImage = user.getUrlImageProfil();
                Log.d("profil", "onDataChange: "+ urlImage + " - "+ namaUser);
                nama.setText(namaUser);

                    Picasso.get()
                            .load(urlImage)
                            .placeholder(R.drawable.boy)
                            .into(imgProfil);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                auth = FirebaseAuth.getInstance();
                auth.signOut();
                Intent intent = new Intent(getContext(), LoginActivity.class);
                getActivity().startActivity(intent);
            }
        });
        return view;

    }


}

