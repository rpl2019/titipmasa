package com.example.android.titipmasa;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import id.titipmasa.adapter.RekomendasiBarangAdapter;
import id.titipmasa.model.RekomendasiBarang;

public class FragmentRekomendasi extends Fragment {
    ArrayList<RekomendasiBarang> rekomendasiBarangs = new ArrayList<>();
    RecyclerView recyclerView;
    DatabaseReference databaseReference;
    FirebaseAuth auth;
    StorageReference storageReference;
    String id_traveller;
    RekomendasiBarangAdapter adapter;
    public FragmentRekomendasi() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_rekomendasi_barang, container, false);

        recyclerView = view.findViewById(R.id.rv_list_rekom);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        databaseReference = FirebaseDatabase.getInstance().getReference();
        Query query = databaseReference.child("RekomendasiBarang").orderByChild("id_tarveller").equalTo(id_traveller);

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){
                    RekomendasiBarang rekom = dataSnapshot1.getValue(RekomendasiBarang.class);

                    rekomendasiBarangs.add(rekom);
                }
                adapter = new RekomendasiBarangAdapter(getContext(), rekomendasiBarangs);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        id_traveller = getActivity().getIntent().getStringExtra("ID_TRAVELLER");
    }
}
