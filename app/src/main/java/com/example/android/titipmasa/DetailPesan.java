package com.example.android.titipmasa;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import id.titipmasa.adapter.PesanAdapter;
import id.titipmasa.model.Pesan;
import id.titipmasa.model.User;

public class DetailPesan extends AppCompatActivity {

    EditText isi_pesan;
    DatabaseReference databaseReference, profil, dfPenerima;
    FirebaseDatabase firebaseDatabase;
    FirebaseAuth mAuth;
    PesanAdapter adapter;
    RecyclerView rv_list_message;
    ArrayList<Pesan> pesans;
    String nama_pengirim, user_id;
    ProgressBar progressBar;
    String imgProfil;
    String  namaPenerima, imgProfilPenerima;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pesan);

        progressBar = findViewById(R.id.progressBar);
        isi_pesan = findViewById(R.id.messageEditText);
        rv_list_message = findViewById(R.id.rv_message);
        rv_list_message.setHasFixedSize(true);
        rv_list_message.setLayoutManager(new LinearLayoutManager(this));
        mAuth = FirebaseAuth.getInstance();

       user_id = mAuth.getCurrentUser().getUid();

        pesans = new ArrayList<>();
        Button btn_send = findViewById(R.id.sendButton);



        final String to_nama = getIntent().getStringExtra("NAMA");
        final String to_user_id = getIntent().getStringExtra("USER_ID");
        Log.d("to_user", "onCreate: "+ to_user_id);
        setTitle(to_nama);

        Log.d("to_user", "onCreate: "+to_user_id);
        final String id_user = getIntent().getStringExtra("ID_USER");

        databaseReference = FirebaseDatabase.getInstance().getReference("Message");
        dfPenerima = FirebaseDatabase.getInstance().getReference("users").child(to_user_id);
        progressBar.setVisibility(ProgressBar.GONE);

        displayUsers();
        displayNamaPenerima();
        loadPesan();

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String id_pesan = databaseReference.push().getKey();
                final String pesan = isi_pesan.getText().toString().trim();
                Pesan isi = new Pesan(id_pesan,user_id, pesan,nama_pengirim, namaPenerima, to_user_id, imgProfil, imgProfilPenerima);

                databaseReference.child(id_pesan).setValue(isi);

                isi_pesan.setText("");
                progressBar.setVisibility(ProgressBar.GONE);
                adapter.notifyDataSetChanged();
                loadPesan();
            }
        });

    }

    public void loadPesan(){
        pesans.clear();
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){
                    Pesan card = dataSnapshot1.getValue(Pesan.class);
                    if (card != null){
                        progressBar.setVisibility(ProgressBar.VISIBLE);
                    }
                    pesans.add(card);
                }
                adapter = new PesanAdapter(DetailPesan.this, pesans);
                adapter.notifyDataSetChanged();
                rv_list_message.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                progressBar.setVisibility(ProgressBar.GONE);
            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void displayUsers(){
        profil = FirebaseDatabase.getInstance().getReference("users").child(user_id);

        profil.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                nama_pengirim = user.getNama();
                imgProfil = user.getUrlImageProfil();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void displayNamaPenerima(){

        dfPenerima.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
               namaPenerima = user.getNama();
                imgProfilPenerima = user.getUrlImageKTP();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
