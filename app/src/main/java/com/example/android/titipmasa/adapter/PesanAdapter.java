package com.example.android.titipmasa.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.android.titipmasa.R;
import com.example.android.titipmasa.model.Pesan;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


public class PesanAdapter extends RecyclerView.Adapter<PesanAdapter.ViewHolderPesan> {

    ArrayList<Pesan> lisPesan;
    Context context;

    public PesanAdapter(Context context, ArrayList<Pesan> lisPesan ) {
        this.lisPesan = lisPesan;
        this.context = context;
    }

    public ArrayList<Pesan> getLisPesan() {
        return lisPesan;
    }


    @NonNull
    @Override
    public ViewHolderPesan onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message, parent, false);

        return new ViewHolderPesan(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderPesan holder, int position) {
        holder.message.setText(getLisPesan().get(position).getIsi_pesan());
        holder.nama_pengirim.setText(getLisPesan().get(position).getFrom_nama());
    }

    @Override
    public int getItemCount() {
        return getLisPesan().size();
    }

    public class ViewHolderPesan extends RecyclerView.ViewHolder {
        TextView message, nama_pengirim;

        public ViewHolderPesan(@NonNull View itemView) {
            super(itemView);
            nama_pengirim = itemView.findViewById(R.id.messageTextView);
            message = itemView.findViewById(R.id.messengerTextView);
        }
    }
}
