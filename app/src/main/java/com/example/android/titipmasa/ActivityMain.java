package com.example.android.titipmasa;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class ActivityMain extends AppCompatActivity {
    private ManagerPref managerPref;
    TextView btnDaftar;
    Button btnLogin;
    TextInputLayout input_layout_useremail, input_layout_password;
    EditText inUsernameEmail, inPassword;
    boolean loginFirebase = false;
    ManagerSession managerSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        getWindow().setStatusBarColor(Color.TRANSPARENT);
        setContentView(R.layout.activity_main);

        managerSession = new ManagerSession(ActivityMain.this);

        /**
         * Layout variables declared start from here.
         */
        btnDaftar              = findViewById(R.id.btnDaftar);
        btnLogin               = findViewById(R.id.btnLogin);
        input_layout_useremail = findViewById(R.id.input_layout_useremail);
        input_layout_password  = findViewById(R.id.input_layout_password);
        inUsernameEmail        = findViewById(R.id.inUsernameEmail);
        inPassword             = findViewById(R.id.inPassword);
        /**
         * Layout variables declared end here.
         */

        inUsernameEmail.addTextChangedListener(new LoginTextWatcher(inUsernameEmail));
        inPassword.addTextChangedListener(new LoginTextWatcher(inPassword));

        managerPref = new ManagerPref(ActivityMain.this);
        if(managerPref.isFirstTimeLaunch()){
            introListener();
        }

        btnLoginListener();
        btnDaftarListener();
    }

    public void btnLoginListener(){
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!validateUser() || !validatePassword()){
                    return;
                }

                FirebaseAuth auth = FirebaseAuth.getInstance();
                new HttpActivityLogin(inUsernameEmail.getText().toString(), inPassword.getText().toString()).execute();
                auth.signInWithEmailAndPassword(inUsernameEmail.getText().toString(), inPassword.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            loginFirebase = true;

                        }
                        else{

                            Toast.makeText(ActivityMain.this,"Invalid email or password",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }

    public void btnDaftarListener(){
        btnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ActivityMain.this, RegisterActivity.class));
            }
        });
    }

    public class HttpActivityLogin extends AsyncTask<String, String, String>{
        ProgressDialog progressDialog;
        private volatile boolean running = true;

        private final static String URL = "https://app.titipmasa.id/public/api/userlogin";
        protected String useremail, password, token, status, message;
        JSONObject jsonObject;
        JSONParser jsonParser;

        public HttpActivityLogin(String useremail, String password){
            this.useremail = useremail;
            this.password  = password;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ActivityMain.this);
            progressDialog.setMessage("Logging in...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            jsonParser = new JSONParser();

            HashMap<String, String> params = new HashMap<>();
            params.put("email", useremail);
            params.put("password", password);

            try {
                jsonObject = jsonParser.makeHttpRequest(URL, "POST", params, "");
                status = jsonObject.getString("status");

                if(!jsonObject.getString("token").isEmpty()){
                    token = jsonObject.getString("token");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if(progressDialog != null && progressDialog.isShowing()){
                progressDialog.dismiss();
            }

            if(status.equals("success") || loginFirebase == true){
                managerSession.createLoginSession(useremail, token);
                Toast.makeText(ActivityMain.this, "Sign in success", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(ActivityMain.this, NavigationBar.class));
                finish();
            }else{
                Toast.makeText(ActivityMain.this, "Sign in failed", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
    }

    public void introListener(){
        managerPref.setFirstTimeLaunch(false);
        startActivity(new Intent(ActivityMain.this, ActivityIntro.class));
        finish();
    }

    private class LoginTextWatcher implements TextWatcher {

        private View view;

        private LoginTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.inUsernameEmail:
                    validateUser();
                    break;
                case R.id.inPassword:
                    validatePassword();
                    break;
            }
        }
    }

    private Boolean validateUser(){
        if (inUsernameEmail.getText().toString().trim().isEmpty()){
            input_layout_useremail.setError("UserID tidak boleh kosong");
            requestFocus(inUsernameEmail);
            return false;
        }else{
            input_layout_useremail.setErrorEnabled(false);
        }
        return true;
    }

    private Boolean validatePassword(){
        if (inPassword.getText().toString().trim().isEmpty()){
            input_layout_password.setError("Password tidak boleh kosong");
            requestFocus(inPassword);
            return false;
        }else if(inPassword.getText().toString().length() >= 20 ){ //Panjang Password
            input_layout_password.setError("Password tidak lebih dari 20 karakter");
            requestFocus(inPassword);
            return false;
        }else{
            input_layout_password.setErrorEnabled(false);
        }
        return true;
    }

    private void requestFocus(View view){
        if(view.requestFocus()){
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
}
