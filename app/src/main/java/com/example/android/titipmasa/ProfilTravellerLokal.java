package com.example.android.titipmasa;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import id.titipmasa.model.DataTrip;

public class ProfilTravellerLokal extends AppCompatActivity {

    TextView nama, tujuan, tgl_kembali;
    String nama_traveller;
    String id_traveller;
    ImageView imgProfil;
    FirebaseAuth auth;
    DatabaseReference mDatabaseRef;
    StorageReference mStorageRef;
    String user_id;
    ArrayList<DataTrip> trips = new ArrayList<>();
    String namaTraveller, tujuanTraveller, tglPulang, imgProfilUrl, lokasiPulang;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil_trip);
        getSupportActionBar().hide();
        nama = findViewById(R.id.tv_nama_traveller);
        tujuan = findViewById(R.id.tv_tujuan_lokal);
        tgl_kembali = findViewById(R.id.tv_tgl_pulang);
        imgProfil = findViewById(R.id.img_profile_tarveller);

        user_id = getIntent().getStringExtra("USER_ID");
        nama_traveller = getIntent().getStringExtra("NAMA_TRAVELLER");
        id_traveller = getIntent().getStringExtra("ID_TRAVELLER");
        String tujuan_traveller = getIntent().getStringExtra("TUJUAN_TRAVELLER");
        String lokasi_traveller = getIntent().getStringExtra("LOKASI_TRAVELLER");
        String kembali_traveller = getIntent().getStringExtra("KEMBALI_TRAVELLER");
        String url_image_profil = getIntent().getStringExtra("IMG_PROFIL");


        Log.d("user_id", "onCreate: " +user_id);
        mDatabaseRef = FirebaseDatabase.getInstance().getReference();

        Picasso.get()
                .load(url_image_profil)
                .placeholder(R.drawable.boy)
                .into(imgProfil);

        nama.setText(nama_traveller);
        tujuan.setText(tujuan_traveller);
        tgl_kembali.setText(kembali_traveller);

        FloatingActionButton fab = findViewById(R.id.fab_pesan_barang);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfilTravellerLokal.this, SendMessageActivity.class);
                intent.putExtra("ID_TRAVELLER", user_id);
                startActivity(intent);
            }
        });

        // Create an instance of the tab layout from the view.
        TabLayout tabLayout = findViewById(R.id.tab_layout);
        // Set the text for each tab.
        tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_label1));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_label2));
//        tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_label4));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_label3));

        // Set the tabs to fill the entire layout.
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        // Use PagerAdapter to manage page views in fragments.
        // Each page is represented by its own fragment.
        final ViewPager viewPager = findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);

        // Setting a listener for clicks.
        viewPager.addOnPageChangeListener(new
                TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(
                new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        viewPager.setCurrentItem(tab.getPosition());

                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {

                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {

                    }
                });

        }

        public void RekomendasiBarang(View view){
            Intent intent = new Intent(ProfilTravellerLokal.this, DetailRekomendasiBarang.class);
            startActivity(intent);
        }

        public void DaftarTitipan(View view){
            Intent intent = new Intent(ProfilTravellerLokal.this, DetailRekomendasiBarang.class);
            startActivity(intent);
        }

        public void Pesan(View view){
            Intent intent = new Intent(ProfilTravellerLokal.this, DetailPesan.class);
            intent.putExtra("NAMA", nama_traveller);
            intent.putExtra("USER_ID", user_id);
            startActivity(intent);
        }

        public void pesan_barang(View view){
            Intent intent = new Intent(ProfilTravellerLokal.this, SendMessageActivity.class);
            startActivity(intent);
        }

        public  void loadData(){
            Query query = mDatabaseRef.child("Trips").orderByChild("user_id").equalTo(user_id);
            query.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    DataTrip trip = dataSnapshot.getValue(DataTrip.class);

                    namaTraveller = trip.getNama();
                    tujuanTraveller =  trip.getTujuan();
                    lokasiPulang = trip.getLokasi();
                    tglPulang = trip.getPulang();
                    imgProfilUrl = trip.getImgProfil();

                }


                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.d("database", "onCancelled: " + databaseError.getMessage());
                }
            });

            Log.d("namaTraveller", "onDataChange: "+ namaTraveller);
            Picasso.get()
                    .load(imgProfilUrl)
                    .placeholder(R.drawable.boy)
                    .into(imgProfil);

            nama.setText(namaTraveller);
            tujuan.setText(tujuanTraveller+" - " + lokasiPulang);
            tgl_kembali.setText(tglPulang);


        }
}



